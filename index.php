<!DOCTYPE html>
<html lang="da">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Problemer med at finde rundt på Navitas? Ikke længere!">
    <meta name="keywords" content="Navigation Navitas">
    <meta name="author" content="NavNav">
    <title>Naviger Navitas - NavNav</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/navnav.css">
    <link rel="image_src" href="/img/navitas.jpeg" />
    <meta property='og:image' content='/img/navitas.jpeg' />
</head>

<body>
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="http://navnav.dk">
                    <p class="nav-logo">
                        <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>NavNav&nbsp;
                        <span class="logo-last">Naviger Navitas</span>
                    </p>
                </a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="/søg">Søg</a></li>
                    <li><a href="/toilet">Toilet</a></li>
                    <li><a href="/print">Print</a></li>
                    <li><a href="/køkken">Køkken</a></li>
                    <li><a href="/om">Om</a></li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="container">
    <?php

    function append_to_file($path, $text)
    {
    	$handle = fopen($path, "a");
    	fwrite($handle, $text);
    	fclose($handle);
    }

    $success = "<input type='button' name='' class='btn btn-large btn-info' value='Af sted!'>";
    $picture = "<img src='/img/navitas.jpeg' style='width:100%' title='Søg efter et lokale ovenfor'>";
    $id = '';
    $floor = '';
    $room = '';
    $permalink = '';
    $showlink = '';
    $tip = '';
    $error = '';
    $time = date("m-d H:i:s");
    $address = $_SERVER['REMOTE_ADDR'];
    $year = date("Y");

    // How to handle direct links
    if ($_SERVER["REQUEST_METHOD"] == "GET" and $_GET["id"]) {
        $id = $_GET["id"];
        if ($id[0] == 0) {
            $floor = $id[1];
        } else {
            $floor = $id[0];
        }
        $room = substr($id, 3, 5);
        $room = ltrim($room, '0');
        $len = strlen($room);

        switch ($len) {
            case 1:
                $long = '00'.$room;
                break;
            case 2:
                $long = '0'.$room;
                break;
            case 3:
                $long = $room;
                break;
        }

        $tip = '<p class="tip">Tip: Tryk på billedet for at forstørre det.</p>';
        $filename = "./img/" . "$floor" . "/" . "$room" . ".jpg";
        if (file_exists($filename)) {
            $picture = "<a href='/img/" . "$floor" . "/" . "$room" . ".jpg'><img src='/img/" . "$floor" . "/" . "$room" . ".jpg' style='width:100%' title='Tryk for fuld skærm'></a>";
            append_to_file("logs/$year.log", $time . ", " . $address . ", L, 0" . $floor . "." . $long . "\n");
        } else {
            $error = '<p class="error">Lokalet findes ikke. Prøv igen.</p>';
        }

    } elseif ($_SERVER["REQUEST_METHOD"] == "GET" and $_GET["err"]) {
        $error = '<p class="error">Lokalet findes ikke. Prøv igen.</p>';
    }

    // How to handle form and search requests
    if ($_SERVER["REQUEST_METHOD"] == "POST" and (htmlentities($_POST['setFloor']) or htmlentities($_POST['roomString']))) {
        if (htmlentities($_POST['setFloor'])) {
            $floor = $_POST['setFloor'];
            $floor = $floor-1 ;
            $room = $_POST['setRoom'];
            $type = "F";
        } else {
            $roomString = $_POST['roomString'];
            $floor = substr($roomString, 1, 1);
            $room = substr($roomString, 3, 5);
            $type = "S";
        }

        // CHECK OM INPUT ER 1, 2 ELLER 3 INTEGERS
        // Sørg for at formatere billed-sti og permalink ordentligt

        preg_match_all('!\d{1,3}!', $room, $matches);
        if ($matches[0]) {
            $room = $matches[0][0];
            $room = ltrim($room, '0');
            $len = strlen($room);

            switch ($len) {
                case 1:
                    $long = '00'.$room;
                    break;
                case 2:
                    $long = '0'.$room;
                    break;
                case 3:
                    $long = $room;
                    break;
            }

            $tip = '<p class="tip">Tip: Tryk på billedet for at forstørre det.</p>';
            $filename = "./img/" . "$floor" . "/" . "$room" . ".jpg";
            if (file_exists($filename)) {
                $picture = "<a href='/img/" . "$floor" . "/" . "$room" . ".jpg'><img src='/img/" . "$floor" . "/" . "$room" . ".jpg' style='width:100%' title='Tryk for fuld skærm'></a>";
                append_to_file("logs/$year.log", $time . ", " . $address . ", " . $type . ", 0" . $floor . "." . $long . "\n");
                if ($type == "F") {
                    $permalink = "http://navnav.dk/0$floor.$long";
                }
            } else {
                $error = '<p class="error">Lokalet findes ikke. Prøv igen.</p>';
            }
        } else {
            $error = '<p class="error">Lokalet findes ikke. Prøv igen.</p>';
        }
    }

    if ($permalink) {
        $showlink =  '
        <div class="form-group permalink">
            <label for="setFloor" class="control-label col-xs-2 col-md-offset-1">Link</label>
            <div class="col-xs-10 col-md-9">
                <input type="text" id="permalink" onClick="SelectAll(\'permalink\');" class="form-control text-box" value="'.$permalink.'">
            </div>
        </div>';
    }
    ?>

        <div class="row center">
            <div class="col-xs-12 col-xs-offset-0 col-sm-6 col-sm-offset-3">
                <div class="input-area">
                    <form class="form-horizontal" method="POST" action="/.">
                        <div class="form-group">
                            <label for="setFloor" class="control-label col-xs-2 col-xs-offset-0 col-md-offset-1">Etage</label>
                            <div class="col-xs-10 col-md-9 radios">
                                <div class="btn-group center" data-toggle="buttons">
                                    <label class="btn btn-default">
                                        <input type="radio" name="setFloor" value="1" autocomplete="off"> 0
                                    </label>
                                    <label class="btn btn-default">
                                        <input type="radio" name="setFloor" value="2" autocomplete="off"> 1
                                    </label>
                                    <label class="btn btn-default">
                                        <input type="radio" name="setFloor" value="3" autocomplete="off"> 2
                                    </label>
                                    <label class="btn btn-default">
                                        <input type="radio" name="setFloor" value="4" autocomplete="off"> 3
                                    </label>
                                    <label class="btn btn-default">
                                        <input type="radio" name="setFloor" value="5" autocomplete="off"> 4
                                    </label>
                                    <label class="btn btn-default">
                                        <input type="radio" name="setFloor" value="6" autocomplete="off"> 5
                                    </label>
                                    <label class="btn btn-default">
                                        <input type="radio" name="setFloor" value="7" autocomplete="off"> 6
                                    </label>
                                    <label class="btn btn-default">
                                        <input type="radio" name="setFloor" value="8" autocomplete="off"> 7
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="setRoom" class="control-label col-xs-2 col-xs-offset-0 col-md-offset-1">Lokale</label>
                            <div class="col-xs-10 col-md-9">
                                <input type="text" class="form-control text-box" id="setRoom" name="setRoom" placeholder="fx 104" required="required">
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-lg btn-primary">Af sted!</button>
                            <? if ($error) {echo $error;} ?>
                        </div>
                        <?php if ($showlink) {echo $showlink;} ?>
                    </form>
                </div>
            </div>
        </div>

        <div class="row bot">
            <div class="col-xs-12 col-md-10 col-md-offset-1">
                <div class="box">
                    <?php
                        echo $picture;
                        echo $tip;
                    ?>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        function SelectAll(id) {
            document.getElementById(id).focus();
            document.getElementById(id).select();
        }
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</body>
</html>
