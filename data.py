import csv
import json

"""
Create a json file with company names and room numbers from a given csv file.
Source: Spreadsheet from INCUBA
"""

csvfile = open('INCUBA.csv', 'r')
jsonfile = open('./data/INCUBA.json', 'w')
read = csv.reader(csvfile, delimiter=',')
dict = {}
for i, row in enumerate(read):
    if i > 0:  # skip header
        if row[0]:  # skip if room field is empty
            if row[4] != 'Aarhus Universitet':  # skip if occupied by AU
                dict[str(row[4])] = [int(row[0][0]), str(row[0][2:])]
json.dump(dict, jsonfile)

"""
Create a json file with laboratory names and room numbers from a given csv file.
Source: http://ase.au.dk/om-ingenioerhoejskolen/laboratorier-og-vaerksteder/
"""

csvfile = open('labs.csv', 'r')
jsonfile = open('./data/labs.json', 'w')
read = csv.reader(csvfile, delimiter=',')
dict = {}
for row in read:
    dict[row[1]] = [int(row[0][0]), str(row[0][2:])]
json.dump(dict, jsonfile)
