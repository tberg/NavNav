<?
$thisYear = date("Y");
?>
<!DOCTYPE html>
<html lang="da">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Problemer med at finde rundt på Navitas? Ikke længere!">
    <meta name="keywords" content="Navigation Navitas">
    <meta name="author" content="NavNav">
    <title>Om - NavNav</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/navnav.css">
    <link rel="image_src" href="/img/navitas.jpeg" />
    <meta property='og:image' content='/img/navitas.jpeg' />
</head>

<body>
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="http://navnav.dk">
                    <p class="nav-logo">
                        <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>NavNav&nbsp;
                        <span class="logo-last">Naviger Navitas</span>
                    </p>
                </a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="/søg">Søg</a></li>
                    <li><a href="/toilet">Toilet</a></li>
                    <li><a href="/print">Print</a></li>
                    <li><a href="/køkken">Køkken</a></li>
                    <li class="active"><a href="/om">Om</a></li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-xs-offset-0">
                <h1 id="om">Om NavNav</h1>
                <hr class="slim-margin">
                <p>
                    NavNav er lavet af <a href="http://mtherkildsen.dk">Marcus Therkildsen</a>, <a href="http://pure.au.dk/portal/da/persons/bo-tranberg(35d7d11f-d146-4ab2-b2db-35e1e253a18a).html">Bo Tranberg</a> og <a href="http://pure.au.dk/portal/da/persons/emil-eriksen(ca052e34-82fb-4d43-9f52-9997b53adbd9).html">Emil Haldrup Eriksen</a> fordi vi blev trætte af ikke at kunne finde rundt på <a href="http://eng.medarbejdere.au.dk/institutlokationer/navitas/">Navitas</a>. Der er tilsyneladende ikke noget system i nummerering af lokaler på tværs af etager (<a href="img/rod.jpg">bevis</a>), så vi har ofte oplevet at gå helt forkert. Vi lavede NavNav som en overspringshandling imens vi arbejdede på vores specialer i foråret 2015.
                </p>
                <p class="play">
                    <a href="https://play.google.com/store/apps/details?id=dk.auesg"><img src="/img/play.png"></a>
                    <a href="http://uniq.cd/3im3q"><img src="/img/App_Store_Badge.svg" style="height:45px;"></a>
                </p>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-xs-offset-0">
                <h1 id="links">Direkte links</h1>
                <hr class="slim-margin">
                <a href="http://navnav.dk/auditorium"><button class="btn btn btn-primary">Auditorium</button></a>&nbsp;
                <a href="http://navnav.dk/bibliotek"><button class="btn btn btn-primary">Bibliotek</button></a>&nbsp;
                <a href="http://navnav.dk/DSR"><button class="btn btn btn-primary">DSR</button></a>&nbsp;
                <a href="http://navnav.dk/FacilityManagement"><button class="btn btn btn-primary">Facility Management</button></a>&nbsp;
                <a href="http://navnav.dk/fitness"><button class="btn btn btn-primary">Fitness</button></a>&nbsp;
                <a href="http://navnav.dk/Haddokks"><button class="btn btn btn-primary">Haddokks</button></a>&nbsp;
                <a href="http://navnav.dk/kantine"><button class="btn btn btn-primary">Kantine</button></a>&nbsp;
                <a href="http://navnav.dk/StudieService"><button class="btn btn btn-primary">Studieservice</button></a>&nbsp;
                <a href="http://navnav.dk/TekniskBoglade"><button class="btn btn btn-primary">Teknisk Boglade</button></a>&nbsp;
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-xs-offset-0">
                <h1 id="stats">Statistik</h1>
                <hr class="slim-margin">
                <p>
                    Her vises de samlede tal for hele NavNavs levetid. Hvis du vil se tallene for hver enkelt år, kan du finde dem <a href="/statistik">her</a>.
                </p>
                <div class="center">
                    <?
                        include 'stats/visits.php';
                        echo '<h3 class="visits">Vi har hjulpet med at finde <b>' . $visits . '</b> lokaler siden maj 2015 <a data-toggle="popover" data-title=""><span class="glyphicon glyphicon-info-sign"></span></a></h3>';
                    ?>
                </div>
                <div class="row center">
                    <div class="col-xs-10 col-xs-offset-1 col-sm-4 col-sm-offset-0 bot">
                        <h4>Mest søgte etager</h4>
                        <img src='stats/floorHist.svg' style='width:100%;' title=''>
                    </div>
                    <div class="col-xs-10 col-xs-offset-1 col-sm-4 col-sm-offset-0 bot">
                        <h4>Tidspunkt for søgninger</h4>
                        <img src='stats/hourHist.svg' style='width:100%;' title=''>
                    </div>
                    <div class="col-xs-10 col-xs-offset-1 col-sm-4 col-sm-offset-0 bot">
                        <h4>Daglige søgninger</h4>
                        <img src='stats/dayHist.svg' style='width:100%;' title=''>
                    </div>
                </div>
                <h2 id="top10">Top 10 mest søgte lokaler</h2>
                <? include 'stats/topRooms.html'; ?>

                <h2>Daglige søgninger</h2>
                <p>
                    Antal søgninger per dag i løbet af året. Se tidligere år <a href="/statistik">her</a>.
                </p>
                <div class="row center">
                    <div class="col-xs-12">
                        <img src='stats/year-<?echo $thisYear;?>.svg' style='width:100%;' title=''>
                    </div>
                </div>
                <div class="center">
                    <?
                    $filename = 'stats/year-'.$thisYear.'.svg';
                    if (file_exists($filename)) {
                        echo "<p class='text-muted status'>Statistikken er senest opdateret: " . date ("j/n H:i.", filemtime($filename)) . "</p>";
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <script type="text/javascript">
	$('[data-toggle="popover"]').popover(
		{
			'trigger': 'hover',
			'placement': 'top',
			'html': true,
			'content': 'Kun søgninger foretaget fra Danmark er talt med.',
		});
    </script>
</body>
</html>
