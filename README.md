# NavNav
Koden bag [NavNav.dk](http://navnav.dk), et værktøj til at navigere bygningen Navitas på Aarhus Universitet.

Projektet er lavet af Marcus Therkildsen, Emil Haldrup Eriksen og Bo Tranberg.

Få den som en app:

[![Google Play](img/play.png)](https://play.google.com/store/apps/details?id=dk.auesg)
[![App Store](img/App_Store_Badge.png)](http://uniq.cd/3im3q)
