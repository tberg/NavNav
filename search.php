<!DOCTYPE html>
<html lang="da">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Problemer med at finde rundt på Navitas? Ikke længere!">
    <meta name="keywords" content="Navigation Navitas">
    <meta name="author" content="NavNav">
    <title>Søg - NavNav</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/navnav.css">
    <link rel="stylesheet" href="/css/search.css">
    <link rel="image_src" href="/img/navitas.jpeg" />
    <meta property='og:image' content='/img/navitas.jpeg' />
</head>

<body>
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="http://navnav.dk">
                    <p class="nav-logo">
                        <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>NavNav&nbsp;
                        <span class="logo-last">Naviger Navitas</span>
                    </p>
                </a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li class="active"><a href="#">Søg</a></li>
                    <li><a href="/Toilet">Toilet</a></li>
                    <li><a href="/Print">Print</a></li>
                    <li><a href="/Køkken">Køkken</a></li>
                    <li><a href="/om">Om</a></li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="container">
        <div id="list">
            <div class="top">
                <input class="search" placeholder="Søg" autofocus>
                <button class="sort" data-sort="name">Sorter efter navn</button>
                <p>
                    Her kan du søge efter <b>ansatte</b>, <b>navngivne lokaler</b>, <b>laboratorier</b> og <b>INCUBA-virksomheder</b>.<br>Vi garanterer ikke at oplysningerne er korrekte, da listen sjældent opdateres.
                </p>
            </div>
            <form method='POST' id="FORM" action='/.'>
                <input type='hidden' id="roomString" name='roomString' value=''>
                <ul class="list">
                    <?php
                        $files = array('ROI', 'INCUBA', 'labs', 'person');

                        foreach ($files as $file) {
                            $contents = file_get_contents('./data/' . $file . '.json');
                            $json = json_decode($contents, true);

                            $jsonIterator = new RecursiveIteratorIterator(
                                new RecursiveArrayIterator($json),
                                RecursiveIteratorIterator::SELF_FIRST);

                            foreach ($jsonIterator as $key => $val) {
                                if(is_array($val)) {
                                    echo "<li class='item' id='0$val[0].$val[1]'>";
                                    echo "<h3 class='name'>$key</h3>";
                                    echo "<p class='room'>0$val[0].$val[1]</p>";
                                    echo "</li>\n";
                                }
                            }
                        }
                    ?>
                </ul>
            </form>
            <div class="center">
                <ul class="pagination"></ul>
            </div>
        </div>
    </div>

    <!-- Script to enable searching in list -->
    <script src="/js/list.js"></script>
    <script src="/js/list.pagination.js"></script>
    <script type="text/javascript">
        var options = {
          valueNames: [ 'name', 'room' ],
          page: 40,
          plugins: [ListPagination({})]
        };
        var userList = new List('list', options);
    </script>

    <!-- Standard stuff -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>

    <!-- Submit an item form, which fetches the desired map -->
    <script type="text/javascript">
        $(document).on('click', '.item', function() {
            str = this.id;
            $('#roomString').val(str);
            $('#FORM').submit();
        });
    </script>
</body>
</html>
