<!DOCTYPE html>
<html lang="da">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Problemer med at finde rundt på Navitas? Ikke længere!">
    <meta name="keywords" content="Navigation Navitas">
    <meta name="author" content="NavNav">
    <title>Printere - NavNav</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/navnav.css">
    <link rel="image_src" href="/img/navitas.jpeg" />
    <meta property='og:image' content='/img/navitas.jpeg' />
</head>

<body>
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="http://navnav.dk">
                    <p class="nav-logo">
                        <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>NavNav&nbsp;
                        <span class="logo-last">Naviger Navitas</span>
                    </p>
                </a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="/søg">Søg</a></li>
                    <li><a href="/toilet">Toilet</a></li>
                    <li class="active"><a href="#">Print</a></li>
                    <li><a href="/køkken">Køkken</a></li>
                    <li><a href="/om">Om</a></li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="container">
    <?php

    function append_to_file($path, $text)
    {
    	$handle = fopen($path, "a");
    	fwrite($handle, $text);
    	fclose($handle);
    }

    $success = "<input type='submit' name='' class='btn btn-large btn-info' value='Af sted!'>";
    $picture = "<img src='/img/navitas.svg' style='width:100%' title='Søg efter et lokale ovenfor'>";
    $id = '';
    $floor = '';
    $tip = '';
    $error = '';
    $time = date("m-d H:i:s");
    $address = $_SERVER['REMOTE_ADDR'];
    $year = date("Y");

    // How to handle form requests
    if ($_SERVER["REQUEST_METHOD"] == "POST" and htmlentities($_POST['setFloor'])) {
        $floor = $_POST['setFloor'];
        $floor = $floor-1 ;

            $tip = '<p class="tip">Tip: Tryk på billedet for at forstørre det.</p>';
            $filename = "./img/print/" . "$floor" . ".jpg";
            if (file_exists($filename)) {
                $picture = "<a href='/img/print/" . "$floor" . ".jpg'><img src='/img/print/" . "$floor" . ".jpg' style='width:100%' title='Tryk for fuld skærm'></a>";
                append_to_file("logs/$year.log", $time . ", " . $address . ", F, 0" . $floor . ".print" . "\n");
            } else {
                $error = '<p class="error">Lokalet findes ikke. Prøv igen.</p>';
            }
        }

    ?>

        <div class="row center">
            <h2>Find printere</h2>
            <p class="center">
                Vælg etage.
            </p>
            <div class="col-xs-12 col-xs-offset-0 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
                <form class="form-horizontal" method="POST" action="/Print">
                    <div class="form-group">
                        <div class="btn-group btn-group-lg center" role="group" aria-label="...">
                            <button type="submit" name="setFloor" value="1" class="btn btn-default">0</button>
                            <button type="submit" name="setFloor" value="2" class="btn btn-default">1</button>
                            <button type="submit" name="setFloor" value="3" class="btn btn-default">2</button>
                            <button type="submit" name="setFloor" value="4" class="btn btn-default">3</button>
                            <button type="submit" name="setFloor" value="5" class="btn btn-default">4</button>
                            <button type="submit" name="setFloor" value="6" class="btn btn-default">5</button>
                            <button type="submit" name="setFloor" value="7" class="btn btn-default">6</button>
                            <button type="submit" name="setFloor" value="8" class="btn btn-default">7</button>
                        </div>
                    </div>
                    <div class="form-group">
                        <? if ($error) {echo $error;} ?>
                    </div>
                </form>
            </div>
        </div>

        <div class="row bot">
            <div class="col-xs-12 col-md-10 col-md-offset-1">
                <div class="box">
                    <?php
                        echo $picture;
                        echo $tip;
                    ?>
                </div>
            </div>
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</body>
</html>
