<?
$startYear = 2015;
$thisYear = date("Y");
?>

<!DOCTYPE html>
<html lang="da">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Problemer med at finde rundt på Navitas? Ikke længere!">
    <meta name="keywords" content="Navigation Navitas">
    <meta name="author" content="NavNav">
    <title>Statistik - NavNav</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/navnav.css">
    <link rel="image_src" href="/img/navitas.jpeg" />
    <meta property='og:image' content='/img/navitas.jpeg' />
</head>

<body>
    <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="http://navnav.dk">
                    <p class="nav-logo">
                        <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>NavNav&nbsp;
                        <span class="logo-last">Naviger Navitas</span>
                    </p>
                </a>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="/søg">Søg</a></li>
                    <li><a href="/toilet">Toilet</a></li>
                    <li><a href="/print">Print</a></li>
                    <li><a href="/køkken">Køkken</a></li>
                    <li class="active"><a href="/om">Om</a></li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-xs-offset-0">
                <h1>Statistik</h1>
                <hr class="slim-margin">
                <p>
                    Her findes statistikken for hvert år siden NavNav startede i maj 2015.
                </p>
                <?
                    for ($y = $startYear; $y <= $thisYear; $y++) {
                        echo '<a href="#'.$y.'"><button class="btn btn-lg btn-primary">'.$y.'</button></a>&nbsp';
                    }
                ?>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12 col-xs-offset-0">

                <? for ($y = $startYear; $y <= $thisYear; $y++) : ?>

                <h1 id="<? echo $y; ?>" class="year-head"><? echo $y; ?>

                <?
                    if ($y > $startYear) {
                        echo '<a href="#" title="Til toppen"><span class="glyphicon glyphicon-chevron-up top-btn" aria-hidden="true"></span></a>';
                    }
                ?>

                </h1>
                <hr class="slim-margin">
                <div class="center">
                    <?
                        include 'stats/visits-'.$y.'.php';
                        echo '<h3 class="visits">Vi har hjulpet med at finde <b>' . $visits . '</b> lokaler i '. $y .' <a data-toggle="popover" data-title=""><span class="glyphicon glyphicon-info-sign"></span></a></h3>';
                    ?>
                </div>
                <div class="row center">
                    <div class="col-xs-10 col-xs-offset-1 col-sm-4 col-sm-offset-0 bot">
                        <h4>Mest søgte etager</h4>
                        <img src='stats/floorHist-<? echo $y; ?>.svg' style='width:100%;' title=''>
                    </div>
                    <div class="col-xs-10 col-xs-offset-1 col-sm-4 col-sm-offset-0 bot">
                        <h4>Tidspunkt for søgninger</h4>
                        <img src='stats/hourHist-<? echo $y; ?>.svg' style='width:100%;' title=''>
                    </div>
                    <div class="col-xs-10 col-xs-offset-1 col-sm-4 col-sm-offset-0 bot">
                        <h4>Daglige søgninger</h4>
                        <img src='stats/dayHist-<? echo $y; ?>.svg' style='width:100%;' title=''>
                    </div>
                </div>
                <h3 id="top10">Top 10 mest søgte lokaler</h3>
                <? include 'stats/topRooms-'.$y.'.html'; ?>

                <h3>Daglige søgninger</h3>
                <div class="row center">
                    <div class="col-xs-12">
                        <img src='stats/year-<?echo $y;?>.svg' style='width:100%;' title=''>
                    </div>
                </div>

                <? endfor ;?>

            </div>
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <script type="text/javascript">
    $('[data-toggle="popover"]').popover(
        {
            'trigger': 'hover',
            'placement': 'top',
            'html': true,
            'content': 'Kun søgninger foretaget fra Danmark er talt med.',
        });
    </script>
</body>
</html>
