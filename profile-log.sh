#!/bin/bash
python -mcProfile -o stats.pstats logger.py
gprof2dot -f pstats stats.pstats | dot -Tpng -o stats.png
rm stats.pstats
