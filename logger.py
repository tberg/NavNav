# -*- coding: utf-8 -*-
from __future__ import division
import numpy as np
from datetime import datetime
import matplotlib
matplotlib.use('Agg')  # For running in a non-interactive shell
import matplotlib.pyplot as plt
from netaddr import *
import json
import csv
from time import clock
import os
import multiprocessing as mp
from functools import partial


"""
Stats for navnav.dk
"""
stats_path = './stats/'
stats_file = 'stats'
log_path = './logs/'
start_year = 2015
this_year = datetime.now().year


def simpleaxis(ax):
    color = 'black'
    for spine in ['top', 'right']:
        ax.spines[spine].set_visible(False)
    for spine in ['bottom', 'left']:
        ax.spines[spine].set_edgecolor(color)
    ax.tick_params(color=color, labelcolor=color)
    ax.get_xaxis().tick_bottom()
    ax.get_yaxis().tick_left()


def dictMerge(newD, oldD):
    """
    Loop over new dict and add keys and values to old dict
    """
    for key in newD.keys():
        if key in oldD.keys():
            oldD[key] += newD[key]
        else:
            oldD[key] = newD[key]
    return oldD


def cleaning_and_parsing(year):
    """
    CLEANING
    """
    ip_set = []
    with open('./data/dk_ip.csv') as csvfile:
        [ip_set.append(row[0]) for row in csv.reader(csvfile, delimiter=',', quotechar='|')]
    ipset = IPSet(ip_set)  # ~ 13 mil. ips

    # Only filter new entries
    try:
        old_log = np.genfromtxt(log_path + str(year) + '_valid.log', delimiter=',', dtype='str', autostrip=True)
        num_row, num_col = old_log.shape
        # last entry
        old_last_entry = old_log[-1, :]
    except Exception:
        old_log = []
        num_row = 0
        num_col = 0
        old_last_entry = 0

    l = np.genfromtxt(log_path + str(year) + '.log', delimiter=',', dtype='str', autostrip=True)
    num_row, num_col = l.shape

    if num_row < 200:
        num_old = num_row
    else:
        num_old = 200
    l_str = np.array([str(l[-num_old + hj, :]) for hj in xrange(num_old)])
    old_last_entry_position_in_log = num_row - num_old + np.where(str(old_last_entry) == l_str)[0]

    if len(old_last_entry_position_in_log) == 0:
        start_point = 0
    else:
        start_point = np.array([old_last_entry_position_in_log + 1])[0, -1]

    old_log = list(old_log)
    new_log = []
    for kk in xrange(start_point, num_row, 1):
        if l[kk, 1] in ipset:
            # Append new filtered entries
            old_log.append([l[kk, i].rstrip() for i in xrange(num_col)])
            new_log.append([l[kk, i].rstrip() for i in xrange(num_col)])

    l = np.array(old_log)
    tot_visits = l.shape[0]
    np.savetxt(log_path + str(year) + '_valid.log', l, delimiter=', ', fmt='%s')

    '''
    PARSING
    '''
    if not os.path.exists(stats_path + stats_file + '-' + str(year) + '.npz'):
        # If this is the first time we are calculating stats
        lines, cols = l.shape
        n_lines = xrange(lines)
        dates = [str(year)[-2:] + '-' + l[i, 0][:5] for i in n_lines]
        hours = [int(l[i, 0][6:8]) for i in n_lines]
        accessTypes = [l[i, 2] for i in n_lines]
        floors = [l[i, 3][1:2] for i in n_lines]
        rooms = [l[i, 3][1:] for i in n_lines]

        yeardays = [datetime.strptime(dates[d], '%y-%m-%d').timetuple().tm_yday for d in range(lines)]
        yeardayCounts = dict((x, yeardays.count(x)) for x in yeardays)
        weekdays = [datetime.strptime(dates[d], '%y-%m-%d').weekday() for d in range(lines)]
        weekdayCounts, weekdayBins = np.histogram(weekdays, bins=range(8), normed=False)
        hourCounts, hourBins = np.histogram(hours, bins=range(25), normed=False)
        AccessTypeCounts = dict((x, accessTypes.count(x)) for x in accessTypes)
        floorCountDict = dict((x, floors.count(x)) for x in floors)
        floorCounts = [floorCountDict[k] / 1 for k in sorted(floorCountDict.keys())]
        roomCounts = dict((room, rooms.count(room)) for room in rooms)
        counts = np.array(roomCounts.values())
        order = counts.argsort()[::-1][:10]
        topRoomIDs = np.array(roomCounts.keys())[order]
        topRooms = counts[order]

        np.savez(stats_path + stats_file + '-' + str(year),
                 yc=yeardayCounts,
                 wc=weekdayCounts,
                 hc=hourCounts,
                 atc=AccessTypeCounts,
                 fc=floorCountDict,
                 rc=roomCounts,
                 visits=tot_visits
                 )
        return None, weekdayBins, weekdayCounts, hourBins, hourCounts, floorCounts, AccessTypeCounts, topRooms, topRoomIDs, tot_visits, yeardayCounts

    elif new_log:
        # only calculate stats for the new entries and add these to previous stats
        stats = np.load(stats_path + stats_file + '-' + str(year) + '.npz')
        yeardayCounts = stats['yc'].item()
        weekdayCounts = stats['wc']
        hourCounts = stats['hc']
        AccessTypeCounts = stats['atc'].item()
        floorCountDict = stats['fc'].item()
        roomCounts = stats['rc'].item()

        new_log = np.array(new_log)
        lines, cols = new_log.shape
        n_lines = xrange(lines)
        dates = [str(year)[-2:] + '-' + new_log[i, 0][:5] for i in n_lines]
        hours = [int(new_log[i, 0][6:8]) for i in n_lines]
        accessTypes = [new_log[i, 2] for i in n_lines]
        floors = [new_log[i, 3][1:2] for i in n_lines]
        rooms = [new_log[i, 3][1:] for i in n_lines]

        yeardays = [datetime.strptime(dates[d], '%y-%m-%d').timetuple().tm_yday for d in range(lines)]
        yeardayCountsNew = dict((x, yeardays.count(x)) for x in yeardays)
        yeardayCounts = dictMerge(yeardayCountsNew, yeardayCounts)
        weekdays = [datetime.strptime(dates[d], '%y-%m-%d').weekday() for d in range(lines)]
        wc, weekdayBins = np.histogram(weekdays, bins=range(8), normed=False)
        weekdayCounts += wc
        hc, hourBins = np.histogram(hours, bins=range(25), normed=False)
        hourCounts += hc
        atc = dict((x, accessTypes.count(x)) for x in accessTypes)
        AccessTypeCounts = dictMerge(atc, AccessTypeCounts)
        floorCountDictNew = dict((x, floors.count(x)) for x in floors)
        floorCountDict = dictMerge(floorCountDictNew, floorCountDict)
        floorCounts = [floorCountDict[k] / 1 for k in sorted(floorCountDict.keys())]
        rc = dict((room, rooms.count(room)) for room in rooms)
        roomCounts = dictMerge(rc, roomCounts)
        counts = np.array(roomCounts.values())
        order = counts.argsort()[::-1][:10]
        topRoomIDs = np.array(roomCounts.keys())[order]
        topRooms = counts[order]

        print str(lines) + " new visits."

        np.savez(stats_path + stats_file + '-' + str(year),
                 yc=yeardayCounts,
                 wc=weekdayCounts,
                 hc=hourCounts,
                 atc=AccessTypeCounts,
                 fc=floorCountDict,
                 rc=roomCounts,
                 visits=tot_visits
                 )
        return None, weekdayBins, weekdayCounts, hourBins, hourCounts, floorCounts, AccessTypeCounts, topRooms, topRoomIDs, tot_visits, yeardayCounts

    else:
        # Stop script if no new visits since last run
        print datetime.strftime(datetime.now(), '%d-%m-%Y %H:%M:%S') + ": No new visits."
        return 'stop', None, None, None, None, None, None, None, None, None, None


def merge_years():
    stats = np.load(stats_path + stats_file + '-' + str(this_year) + '.npz')
    yeardayCounts = stats['yc'].item()
    weekdayCounts = stats['wc']
    hourCounts = stats['hc']
    AccessTypeCounts = stats['atc'].item()
    floorCountDict = stats['fc'].item()
    roomCounts = stats['rc'].item()
    tot_visits = stats['visits']

    for y in range(start_year, this_year):
        tmp_stats = np.load(stats_path + stats_file + '-' + str(y) + '.npz')
        tmp_weekdayCounts = tmp_stats['wc']
        tmp_hourCounts = tmp_stats['hc']
        tmp_AccessTypeCounts = tmp_stats['atc'].item()
        tmp_floorCountDict = tmp_stats['fc'].item()
        tmp_roomCounts = tmp_stats['rc'].item()
        tmp_visits = tmp_stats['visits']

        weekdayCounts += tmp_weekdayCounts
        hourCounts += tmp_hourCounts
        AccessTypeCounts = dictMerge(tmp_AccessTypeCounts, AccessTypeCounts)
        floorCountDict = dictMerge(tmp_floorCountDict, floorCountDict)
        roomCounts = dictMerge(tmp_roomCounts, roomCounts)
        tot_visits += tmp_visits

    floorCounts = [floorCountDict[k] / 1 for k in sorted(floorCountDict.keys())]
    counts = np.array(roomCounts.values())
    order = counts.argsort()[::-1][:10]
    topRoomIDs = np.array(roomCounts.keys())[order]
    topRooms = counts[order]

    np.savez(stats_path + stats_file,
             yc=yeardayCounts,
             wc=weekdayCounts,
             hc=hourCounts,
             atc=AccessTypeCounts,
             fc=floorCountDict,
             rc=roomCounts
             )
    return weekdayCounts, hourCounts, floorCounts, AccessTypeCounts, topRooms, topRoomIDs, tot_visits


def daily_hist(weekdayCounts, weekdayBins=range(8), total=False):
    """
    Daily histogram
    """
    plt.figure(figsize=(8, 5))
    ax = plt.subplot(111)
    plt.bar(weekdayBins[:-1], weekdayCounts, width=1, color='#337AB7', edgecolor='#337AB7')
    ax.set_xticks(np.linspace(.6, 6.6, 7))
    ax.set_xticklabels(['mandag', 'tirsdag', 'onsdag', 'torsdag', 'fredag', u'lørdag', u'søndag'], rotation=60, ha='right', va='top', fontsize=12)
    ax.xaxis.grid(False)
    ax.xaxis.set_tick_params(width=0)
    simpleaxis(ax)
    if total:
        plt.savefig(stats_path + '/dayHist.svg', bbox_inches='tight')
    else:
        plt.savefig(stats_path + '/dayHist-' + str(year) + '.svg', bbox_inches='tight')


def hourly_hist(hourCounts, hourBins=range(25), total=False):
    """
    Hourly histogram
    """
    plt.figure(figsize=(8, 5))
    ax = plt.subplot(111)
    plt.bar(hourBins[:-1], hourCounts, width=1, color='#337AB7', edgecolor='#337AB7')
    ax.set_xticks(range(25))
    plt.axis([0, 24, 0, 1.05 * max(hourCounts)])
    simpleaxis(ax)
    if total:
        plt.savefig(stats_path + '/hourHist.svg', bbox_inches='tight')
    else:
        plt.savefig(stats_path + '/hourHist-' + str(year) + '.svg', bbox_inches='tight')


def floor_hist(floorCounts, total=False):
    """
    Floor histogram
    """
    plt.figure(figsize=(8, 5))
    ax = plt.subplot(111)
    plt.bar(range(8), floorCounts, width=1, color='#337AB7', edgecolor='#337AB7')
    ax.set_xticks(np.linspace(.5, 7.5, 8))
    ax.set_xticklabels(range(8), ha='right', va='top', fontsize=10.5)
    ax.xaxis.grid(False)
    ax.xaxis.set_tick_params(width=0)
    simpleaxis(ax)
    if total:
        plt.savefig(stats_path + '/floorHist.svg', bbox_inches='tight')
    else:
        plt.savefig(stats_path + '/floorHist-' + str(year) + '.svg', bbox_inches='tight')


def access_hist(AccessTypeCounts, total=False):
    """
    Access histogram
    """
    plt.figure(figsize=(8, 5))
    ax = plt.subplot(111)
    values = []
    labels = []
    for k in AccessTypeCounts:
        values.append(AccessTypeCounts[k])
        if k == 'S':
            k += u'øg'
        elif k == 'F':
            k += 'orm'
        else:
            k += 'ink'
        labels.append(k)
    plt.bar(range(3), values[::-1], width=1, color='#337AB7', edgecolor='#337AB7')
    ax.set_xticks(np.linspace(.5, 2.5, 3))
    ax.set_xticklabels(labels[::-1], ha='right', va='top', fontsize=10.5)
    ax.xaxis.grid(False)
    ax.xaxis.set_tick_params(width=0)
    simpleaxis(ax)
    if total:
        plt.savefig(stats_path + '/access.svg', bbox_inches='tight')
    else:
        plt.savefig(stats_path + '/access-' + str(year) + '.svg', bbox_inches='tight')


def top_ten(topRooms, topRoomIDs, tot_visits, total=False):
    """
    Top 10 rooms and total number of searches
    """
    with open('./data/ROI.json') as data_file:
        data1 = json.load(data_file)
    with open('./data/person.json') as data_file:
        data2 = json.load(data_file)
    with open('./data/labs.json') as data_file:
        data3 = json.load(data_file)
    with open('./data/INCUBA.json') as data_file:
        data4 = json.load(data_file)
    data = {key: value for (key, value) in (data1.items() + data2.items() + data3.items() + data4.items())}
    roomString = u'<table class="table">\n<thead><tr><th>#</th><th>Lokale</th><th>S&#248;gninger</th><th>Navn</th></tr></thead>\n'
    for i, r in enumerate(topRooms):
        roomString += '<tr><td>' + str(i + 1) + '</td><td>' + topRoomIDs[i] + '</td><td>' + str(r) + '</td><td>'
        for d in data.keys():
            if str(data[d][0]) == topRoomIDs[i][0]:
                if data[d][1] == topRoomIDs[i][2:]:
                    roomString += '<a href="http://navnav.dk/0' + topRoomIDs[i] + '">' + d + '</a>'
        roomString += '</td></tr>\n'
    roomString += '</table>'

    if total:
        text_file = open(stats_path + '/topRooms.html', 'w')
    else:
        text_file = open(stats_path + '/topRooms-' + str(year) + '.html', 'w')
    text_file.write(roomString)
    text_file.close()

    varString = '<?php\n'
    varString += '$visits = ' + str(tot_visits) + ';\n'
    varString += '?>'
    if total:
        text_file = open(stats_path + '/visits.php', 'w')
    else:
        text_file = open(stats_path + '/visits-' + str(year) + '.php', 'w')
    text_file.write(varString)
    text_file.close()


def yearly_hist(yeardayCounts):
    """
    Yearly histogram
    """
    plt.figure(figsize=(14, 5))
    ax = plt.subplot(111)
    if this_year % 4 == 0:
        y = np.zeros(366)
        ax.set_xticks([0, 31, 60, 91, 121, 152, 182, 213, 244, 274, 305, 335])
    else:
        y = np.zeros(365)
        ax.set_xticks([0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334])
    for key in yeardayCounts:
        y[key - 1] = yeardayCounts[key]
    plt.bar(range(len(y)), y, bottom=np.ones(len(y)), color='#337AB7', edgecolor='#337AB7', log=True)
    plt.axis([0, 364, 1, np.max(y)])
    yticks = [1, 10, 100, 1000]
    ax.set_yticks(yticks)
    ax.set_yticklabels(yticks)
    months = [' jan', 'feb', 'mar', ' apr', ' maj', ' jun', ' jul', 'aug', 'sep', ' okt', 'nov', 'dec']
    ax.set_xticklabels([7 * ' ' + month for month in months], ha='left', va='top', fontsize=10)
    simpleaxis(ax)
    plt.savefig(stats_path + '/year-' + str(year) + '.svg', bbox_inches='tight')


def para(weekdayBins, weekdayCounts, hourBins, hourCounts, floorCounts, AccessTypeCounts, topRooms, topRoomIDs, tot_visits, yeardayCounts, x):
    if x == 0:
        daily_hist(weekdayCounts)
    elif x == 1:
        hourly_hist(hourCounts)
    elif x == 2:
        floor_hist(floorCounts)
    elif x == 3:
        access_hist(AccessTypeCounts)
    elif x == 4:
        top_ten(topRooms, topRoomIDs, tot_visits)
    elif x == 5:
        yearly_hist(yeardayCounts)


def main():
    global year
    for year in range(start_year, this_year + 1):
        status, weekdayBins, weekdayCounts, hourBins, hourCounts, floorCounts, AccessTypeCounts, topRooms, topRoomIDs, tot_visits, yeardayCounts = cleaning_and_parsing(year)
        if status == 'stop':
            pass
        else:
            para_or_not = 1
            if para_or_not == 1:
                iterable = xrange(6)
                cores = mp.cpu_count()
                pool = mp.Pool(processes=cores)
                func = partial(para, weekdayBins, weekdayCounts, hourBins, hourCounts, floorCounts, AccessTypeCounts, topRooms, topRoomIDs, tot_visits, yeardayCounts)
                pool.map(func, iterable)
                pool.close()
                pool.join()
            else:
                daily_hist(weekdayCounts)
                hourly_hist(hourCounts)
                floor_hist(floorCounts)
                access_hist(AccessTypeCounts)
                top_ten(topRooms, topRoomIDs, tot_visits)
                yearly_hist(yeardayCounts)

        # Merge stats for /om/
        if year == this_year:
            weekdayCounts, hourCounts, floorCounts, AccessTypeCounts, topRooms, topRoomIDs, tot_visits = merge_years()
            daily_hist(weekdayCounts, total=True)
            hourly_hist(hourCounts, total=True)
            floor_hist(floorCounts, total=True)
            access_hist(AccessTypeCounts, total=True)
            top_ten(topRooms, topRoomIDs, tot_visits, total=True)

if __name__ == '__main__':

    start = clock()

    main()

    print 'Done in ' + str(round(clock() - start, 2)) + ' seconds\n'
